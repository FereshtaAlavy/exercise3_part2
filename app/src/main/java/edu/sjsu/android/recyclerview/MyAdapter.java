package edu.sjsu.android.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<String> values;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtHeader;
        public TextView txtFooter;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            txtHeader = (TextView) v.findViewById(R.id.firstLine);
            txtFooter = (TextView) v.findViewById(R.id.secondLine);
        }
    }

        public void add(int position,String item){
            values.add(position,item);
            notifyItemInserted(position);
        }

        public void remove(int position){
            values.remove(position);
            notifyItemRemoved(position);
        }

        public MyAdapter(List<String> myDataset){
            values=myDataset;
        }

        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(
                ViewGroup parent,int viewType){
            LayoutInflater inflater= LayoutInflater.from(parent.getContext());
            View v= inflater.inflate(R.layout.row_layout,parent,false);
            ViewHolder vh =new ViewHolder(v);
            return vh;

        }

        @Override
        public void onBindViewHolder(ViewHolder holder,final int position){
            // -get element from your dataset at this position
            // -replace the contents of the view with that element
            final String name=values.get(position);
            holder.txtHeader.setText(name);
            holder.txtHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    remove(position);
                }
            });

            holder.txtFooter.setText("Footer: " + name);}

    @Override
    public int getItemCount(){
    return values.size();
    }
}



